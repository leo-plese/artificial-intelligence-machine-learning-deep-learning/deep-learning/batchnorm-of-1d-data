import torch.nn as nn
import torch
from torch.optim import SGD, Adam
from torch.optim.lr_scheduler import ExponentialLR
import numpy as np
import mnist_shootout
from math import ceil
import copy

def batchnorm1d(inp, gammas, betas, mov_means_list, mov_vars_list, hid_layer_ind, training=True, eps=1e-5, momentum=0.1):
    if training:
        # 1. calculate normalized output
        means = torch.mean(inp, dim=0)
        vars = torch.mean((inp - means) ** 2, dim=0)

        inp_normalized = (inp - means) / torch.sqrt(vars + eps)

        # 2. update moving statistics (means - mov_means_list, vars - mov_vars_list)
        if mov_means_list[hid_layer_ind] is None:
            mov_means_list[hid_layer_ind] = means
        else:
            mov_means_list[hid_layer_ind] = (1 - momentum) * means + momentum * mov_means_list[hid_layer_ind]

        if mov_vars_list[hid_layer_ind] is None:
            mov_vars_list[hid_layer_ind] = vars
        else:
            mov_vars_list[hid_layer_ind] = (1 - momentum) * vars + momentum * mov_vars_list[hid_layer_ind]

    else:
        # 1. calculate normalized output
        inp_normalized = (inp - mov_means_list[hid_layer_ind]) / torch.sqrt(mov_vars_list[hid_layer_ind] + eps)

    outp = inp_normalized * gammas + betas

    return outp



class PTDeep(nn.Module):

    def __init__(self, layer_width_list, f_activ, use_bn=False):
        """Arguments:
           - D: dimensions of each datapoint
           - C: number of classes
        """
        super().__init__()

        # inicijalizirati parametre (koristite nn.Parameter):
        # imena mogu biti self.W, self.b
        # ...
        n_wt_layers = len(layer_width_list)
        self.weights = nn.ParameterList(
            [nn.Parameter(torch.randn(layer_width_list[i], layer_width_list[i - 1])) for i in range(1, n_wt_layers)])
        self.biases = nn.ParameterList(
            [nn.Parameter(torch.zeros((layer_width_list[i], 1))) for i in range(1, n_wt_layers)])

        self.use_bn = use_bn
        if use_bn:
            self.gammas = nn.ParameterList([nn.Parameter(torch.ones(layer_width_list[i])) for i in range(1, n_wt_layers-1)])
            self.betas = nn.ParameterList([nn.Parameter(torch.zeros(layer_width_list[i])) for i in range(1, n_wt_layers-1)])
            self.mov_means_list, self.mov_vars_list = [None]*(n_wt_layers-2), [None]*(n_wt_layers-2)

        self.f = f_activ

    def count_params(self):
        num_params = 0
        for p in self.named_parameters():
            print(p[0] + " : " + str(list(p[1].shape)))
            num_params += torch.numel(p[1])
        return num_params

    def forward(self, X):
        # unaprijedni prolaz modela: izračunati vjerojatnosti
        #   koristiti: torch.mm, torch.softmax
        # ...
        scores = (torch.mm(self.weights[0], X.T) + self.biases[0]).T  # N x C
        for i in range(1, len(self.weights)):
            if self.use_bn:
                scores = batchnorm1d(scores, self.gammas[i-1], self.betas[i-1], self.mov_means_list, self.mov_vars_list, i-1, training=self.training)
            h = self.f(scores)
            scores = (torch.mm(self.weights[i], h.T) + self.biases[i]).T
        probs = torch.softmax(scores, dim=1)

        return probs

    def get_loss(self, X, Yoh_, param_lambda=0):
        # formulacija gubitka
        #   koristiti: torch.log, torch.mean, torch.sum

        probs = self.forward(X)  # N x C
        # logaritmirane vjerojatnosti razreda
        logprobs = torch.log(probs+1e-10)  # N x C

        # gubitak
        logprobs_correct_class = (logprobs * Yoh_).flatten()

        loss = -1 / len(X) * torch.sum(logprobs_correct_class) + param_lambda * torch.sum(
            torch.stack([torch.sum(torch.square(wts_i)) for wts_i in self.weights]))  # scalar

        return loss

def train_mb(model, X, Yoh_, param_nepoch, param_delta, param_lambda=0, train_batch_size=8, val_batch_size=2, X_val=None, y_val=None, Yoh_val=None, save_loss_path=None, patience=None, optimizer=None, variable_lr=False):
    """Arguments:
        - X: model inputs [NxD], type: torch.Tensor
       - Yoh_: ground truth [NxC], type: torch.Tensor
       - param_niter: number of training iterations
        - param_delta: learning rate
    """
    N_train, N_val = len(X), len(X_val)
    X_val_for_eval = X_val.reshape(N_val, -1)

    train_num_batches = ceil(N_train / train_batch_size)
    val_num_batches = ceil(N_val / val_batch_size)

    scheduler = None
    # inicijalizacija optimizatora
    if not optimizer:
        optimizer = SGD(model.parameters(), lr=param_delta)  # , weight_decay=param_lambda)

    elif optimizer=="adam":
        optimizer = Adam(model.parameters(), lr=param_delta)
        if variable_lr:
            scheduler = ExponentialLR(optimizer, gamma=1-param_delta)

    if patience:
        last_loss = float("inf")
        loss_counter = 0
        best_n_epoch = param_nepoch

    best_weights, best_biases, best_loss = copy.deepcopy(model.weights), copy.deepcopy(model.biases), None

    losses_list, val_losses_list = [], []

    # petlja učenja
    # ispisujte gubitak tijekom učenja
    for epoch in range(param_nepoch):
        print("######################## EPOCH", epoch, " ########################")

        # train
        print("********* TRAIN *********")
        train_inds = torch.randperm(N_train)
        X, Yoh_ = X[train_inds], Yoh_[train_inds]

        train_loss = 0

        for i in range(train_num_batches):
            torch.cuda.empty_cache()
            data_from_ind, data_to_ind = i*train_batch_size, min((i+1)*train_batch_size, N_train)
            loss = model.get_loss(X[data_from_ind:data_to_ind], Yoh_[data_from_ind:data_to_ind], param_lambda)

            loss.backward()

            optimizer.step()
            optimizer.zero_grad()

            train_loss += loss/train_num_batches

            if i % 500 == 0:
                print("iter {}: {}".format(i, loss))

        # val
        print()
        print("********* VAL *********")
        val_inds = torch.randperm(N_val)
        X_val, Yoh_val = X_val[val_inds], Yoh_val[val_inds]

        val_loss = 0


        with torch.no_grad():
            model.eval()
            for i in range(val_num_batches):
                torch.cuda.empty_cache()
                data_from_ind, data_to_ind = i * val_batch_size, min((i + 1) * val_batch_size, N_val)

                loss = model.get_loss(X_val[data_from_ind:data_to_ind], Yoh_val[data_from_ind:data_to_ind], param_lambda)

                val_loss += loss/val_num_batches

                if i % 500 == 0:
                    print("iter {}: {}".format(i, loss))
            model.train()

        if patience:
            print("escnt", loss_counter)  # early stopping counter
            if loss_counter == patience:
                break

            val_losses_list.append(val_loss)

            cur_loss = val_loss
            if cur_loss < last_loss:
                best_n_epoch = epoch
                best_weights, best_biases = copy.deepcopy(model.weights), copy.deepcopy(model.biases)
                best_loss = cur_loss

                last_loss = cur_loss
                loss_counter = 0
            else:
                loss_counter += 1
                print("!!!!!!! epoch {}: escnt = {}".format(epoch, loss_counter))
        else:
            val_losses_list.append(val_loss)

        losses_list.append(train_loss)

        print("epoch {}: train loss {}, val loss {}".format(epoch, train_loss, val_loss))


        with torch.no_grad():
            model.eval()

            C = Yoh_val.shape[1]

            torch.cuda.empty_cache()

            A_val, P_val, R_val = mnist_shootout.evaluate_model(model, N_val, C, X_val_for_eval, y_val)

            print("A =", A_val)
            print("P =", P_val)
            print("R =", R_val)

            model.train()

        if scheduler:
            scheduler.step()

    if patience:
        print("*********** best_n_epoch =",best_n_epoch, " ***********")
    if save_loss_path:
        np.savetxt(save_loss_path[0], np.array(losses_list))
        np.savetxt(save_loss_path[1], np.array(val_losses_list))

    if not patience:
        best_weights, best_biases = model.weights, model.biases
        best_loss = val_loss
    else:
        if best_loss is None:
            best_loss = val_loss

    return [wts_i.cpu().detach().numpy() for wts_i in best_weights], [b_i.cpu().detach().numpy() for b_i in best_biases], best_loss.cpu().detach().numpy()

def eval(model, X):
    """Arguments:
        - model: type: PTLogreg
       - X: actual datapoints [NxD], type: np.array
       Returns: predicted class probabilites [NxC], type: np.array
    """
    # ulaz je potrebno pretvoriti u torch.Tensor
    # izlaze je potrebno pretvoriti u numpy.array
    # koristite torch.Tensor.detach() i torch.Tensor.numpy()

    X = torch.tensor(X, dtype=torch.float32)
    probs = model.forward(X)
    return probs.cpu().detach().numpy()


