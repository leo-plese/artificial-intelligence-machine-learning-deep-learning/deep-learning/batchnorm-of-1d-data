Batchnorm of 1D data. Implemented in Python using NumPy, PyTorch and Matplotlib library.

My lab assignment in Deep Learning, FER, Zagreb.

Task description in "TaskSpecification.pdf" in section "8. Normalizacija po podatcima (bonus)".

Docs in "sadrzaj.txt"

Created: 2021
