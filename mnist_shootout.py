import torch
import torchvision
import matplotlib.pyplot as plt
import numpy as np
from sklearn.svm import SVC
import pt_deep
import data


def evaluate_model(ptd, N, C, X, Y_):
    # dohvati vjerojatnosti na skupu za učenje
    probs = pt_deep.eval(ptd, X)

    # ispiši performansu (preciznost i odziv po razredima)
    # recover the predicted classes Y
    Y = np.argmax(probs, axis=1).reshape((N, -1))

    # # evaluate and print performance measures
    conf_mats_by_cls, _, accuracy_avg, precision_avg, recall_avg, _, precision_by_cls, _ = data.eval_perf_multi(
        Y, Y_, N, C)

    return accuracy_avg, precision_avg, recall_avg


if __name__ == "__main__":
    np.random.seed(100)
    torch.manual_seed(100)

    dataset_root = r".\data"  # change this to your preference
    mnist_train = torchvision.datasets.MNIST(dataset_root, train=True, download=False)
    mnist_test = torchvision.datasets.MNIST(dataset_root, train=False, download=False)

    x_train, y_train = mnist_train.data, mnist_train.targets
    x_test, y_test = mnist_test.data, mnist_test.targets
    # print(x_train.max().item(), x_test.max().item())
    x_train, x_test = x_train.float().div_(255.0), x_test.float().div_(255.0)
    # print(x_train.max().item(), x_test.max().item())

    N = x_train.shape[0]
    N_test = x_test.shape[0]
    D1, D2 = x_train.shape[1], x_train.shape[2]
    D = D1 * D2
    C = y_train.max().add_(1).item()
    print()

    ###############
    Yoh_ = torch.tensor(data.get_one_hot_matrix(y_train.flatten(), N, C))

    optimizers = ["sgd", "adam"]    # adam is with variable lr = True
    LAMBDAS = [0, 1e-4, 1e-3, 1e-2, 1e-1]
    layers_width_list = [[784, 10], [784, 100, 10], [784, 100, 100, 10], [784, 100, 100, 100, 10]]

    SAVE_LOSS_BASE_PATH = r"..\deep_losses"
    SAVE_TRAIN_LOSS_BASE_PATH = SAVE_LOSS_BASE_PATH + "\\train"
    SAVE_VAL_LOSS_BASE_PATH = SAVE_LOSS_BASE_PATH + "\\val"
    SAVE_A_P_R_BASE_PATH = r"..\deep_apr"
    SAVE_A_P_R_BASE_TRAIN_PATH = SAVE_A_P_R_BASE_PATH + "\\train"
    SAVE_A_P_R_BASE_TEST_PATH = SAVE_A_P_R_BASE_PATH + "\\test"

    n_iter_list = [6000, 9000, 12000, 15000]
    lr_list = [0.1, 0.09, 0.06, 0.04]

    lr_adam = 1e-4

    for optimizer in optimizers:
        print()
        print()
        print()
        print()
        print()
        print()
        print(".........................................................")
        print("..................... OPTIMIZER {} .....................".format(optimizer))
        print(".........................................................")
        for i in range(len(layers_width_list)):
            lr = lr_list[i] if optimizer=="sgd" else lr_adam
            n_iter = n_iter_list[i]
            layers_width = layers_width_list[i]

            print()
            print()
            print()
            print()
            print("############################", layers_width, " ############################")

            val_to_ind = int(0.2 * N)
            perm = np.random.permutation(N)
            x_val, y_val = x_train[perm][:val_to_ind], y_train[perm][:val_to_ind]
            x_train_new, y_train_new = x_train[perm][val_to_ind:], y_train[perm][val_to_ind:]
            Yoh_val, Yoh_train_new = Yoh_[perm][:val_to_ind], Yoh_[perm][val_to_ind:]
            N_train_new, N_val = len(x_train_new), len(x_val)

            x_train_go, Yoh_go = x_train_new.reshape(N_train_new, -1).cuda(), Yoh_train_new.cuda()

            for reg_factor in LAMBDAS:
                print()
                print()
                print("*************** lambda =", reg_factor, "***************")
                for use_bn in [True, False]:
                    print("BATCHNORM =", use_bn)
                    ptd = pt_deep.PTDeep(layers_width, torch.relu, use_bn=use_bn).cuda()

                    ######### TRAIN
                    ptd.train()
                    if optimizer=="sgd":
                        W, b, loss = pt_deep.train_mb(ptd, x_train_go, Yoh_go, n_iter, lr, reg_factor,
                                                      train_batch_size=64, val_batch_size=16,
                                                      X_val=x_val.reshape(N_val, -1).cuda(), y_val=y_val,
                                                      Yoh_val=Yoh_val.cuda(),
                                                      save_loss_path=(
                                                          SAVE_TRAIN_LOSS_BASE_PATH + "\\" + optimizer+ "\\"+str(len(layers_width)) + "_" + str(
                                                              reg_factor) + "_" + str(use_bn) + ".txt",
                                                          SAVE_VAL_LOSS_BASE_PATH + "\\" +  optimizer+ "\\"+str(len(layers_width)) + "_" + str(
                                                              reg_factor) + "_" + str(use_bn) + ".txt"),
                                                      patience=20)
                    else:   # "adam"
                        W, b, loss = pt_deep.train_mb(ptd, x_train_go, Yoh_go, n_iter, lr, reg_factor,
                                                      train_batch_size=64, val_batch_size=16,
                                                      X_val=x_val.reshape(N_val, -1).cuda(), y_val=y_val,
                                                      Yoh_val=Yoh_val.cuda(),
                                                      save_loss_path=(
                                                          SAVE_TRAIN_LOSS_BASE_PATH + "\\" + optimizer+ "\\"+ str(
                                                              len(layers_width)) + "_" + str(
                                                              reg_factor) + "_" + str(use_bn) + ".txt",
                                                          SAVE_VAL_LOSS_BASE_PATH + "\\" + optimizer+ "\\"+ str(
                                                              len(layers_width)) + "_" + str(
                                                              reg_factor) + "_" + str(use_bn) + ".txt"),
                                                      patience=20, optimizer="adam", variable_lr=True)

                    ######### EVAL
                    ptd.eval()
                    A_train, P_train, R_train = evaluate_model(ptd, N, C, x_train.reshape(N, -1).cuda(), y_train)
                    A_test, P_test, R_test = evaluate_model(ptd, N_test, C, x_test.reshape(N_test, -1).cuda(), y_test)

                    print()
                    print("loss = ", loss)
                    print("TRAIN: A = {}, P = {}, R = {}".format(A_train, P_train, R_train))
                    print("TEST: A = {}, P = {}, R = {}".format(A_test, P_test, R_test))

                    np.savetxt(SAVE_A_P_R_BASE_TRAIN_PATH + "\\" +  optimizer+ "\\"+str(len(layers_width)) + "_" + str(
                        reg_factor) + "_" + str(use_bn) + ".out", np.array([A_train, P_train, R_train]))
                    np.savetxt(SAVE_A_P_R_BASE_TEST_PATH + "\\" +  optimizer+ "\\"+str(len(layers_width)) + "_" + str(
                        reg_factor) + "_" + str(use_bn) + ".out", np.array([A_test, P_test, R_test]))

                    print()